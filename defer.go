package main

import "fmt"

func def(){
	fmt.Println("menjalankan defer")
	message := recover();
	if message != nil {
		fmt.Println("terjadi error: ", message)	
	} else{
		fmt.Println("applikasi selesai")
	}
}
func logging(value bool){
	defer def()
	fmt.Println("menjalankan logging")
	if(value){
		panic("applikasi berhenti")
	}
	
	def()
}
func main(){
	logging(false)
}