package main

import "fmt"

func main() {
	// conversion type data
	var a int32 = 32839
	var b int64 = int64(a)
	var c int8 = int8(a)
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	var name = "yohan"
	var e = name[0]
	var eString = string(e)
	fmt.Println((eString))
}
