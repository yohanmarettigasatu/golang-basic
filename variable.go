package main

import "fmt"

func main() {
	// declaration variable with var
	var name string
	var age int
	var isMan = true
	var (
		firstName = "yohan"
		lastName = "martisa"
	)
	fmt.Println(firstName, lastName)
	name = "yohan"
	age = 10
	fmt.Println(name, age, "is a man?", isMan)
	name = "luna"
	age = 11
	isMan = false
	fmt.Println(name, age, "is a man?",isMan)
	// declaration variable without var
	country := "indonesia"
	fmt.Println(country)
	country = "rusia"
	fmt.Println(country)
	// array just can contain same type data with range and cannot add data in array like js
	var arr[3] string
	arr[0]= "a"
	arr[1]= "b"
	arr[2]= "c"
	var arr2 = [3]int{
		1, 2, 3,
	}
	fmt.Println(arr[0], arr[1], arr[2])
}
