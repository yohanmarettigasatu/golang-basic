package main

import "fmt"

func main() {
	// declaration constanta
	const firstName string = "yohan"
	const lastName = "martisa"
	const age = 3
	fmt.Println(firstName, lastName, age)
	// multiple constanta
	const (
		phi     = 3.14
		country = "indonesia"
	)
	fmt.Println(phi, country)
}
