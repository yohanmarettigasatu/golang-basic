package main

import "fmt"
type Blacklist func(string) bool

func registerUser(name string, blacklist Blacklist) string{
	if(blacklist(name)){
		return "your account named "+name+ " have access"
	} else{
		return "your account named "+name+ " blocked"
	}
}
func main(){
	anonymousFunc := func(name string) bool{
		return name == "admin" || name == "root"
	}
	fmt.Println(registerUser("yohan", anonymousFunc))
	fmt.Println(registerUser("admin", anonymousFunc))
	fmt.Println(registerUser("root", func(name string)bool{
		return name == "admin" || name == "root"
	}))
}