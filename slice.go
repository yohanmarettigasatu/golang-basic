package main

import "fmt"

func main() {
	//make slice before initiated
	newSlice := make([]string, 2, 5)
	//make slice and initiated
	// var arr = []string{
	// 	"januari",
	// 	"februari",
	// 	"maret",
	// 	"april",
	// 	"mei",
	// 	"juni",
	// 	"juli",
	// 	"agustus",
	// 	"september",
	// 	"oktober",
	// 	"november",
	// 	"desember",
	// }
	//make arr and initiated
	var arr = [...]string{
		"januari",
		"februari",
		"maret",
		"april",
		"mei",
		"juni",
		"juli",
		"agustus",
		"september",
		"oktober",
		"november",
		"desember",
	}
	var slice1 = arr[2:7]
	// var slice2 = arr[:9]
	// var slice3 = arr[4:9]
	// var slice4 = arr[:]
	// fmt.Println(slice1)
	// fmt.Println(slice2)
	// fmt.Println(slice3)
	// fmt.Println(slice4)
	var sliceAppend = append(slice1, "data")
	// fungsi append jika array gk sanggup maka bikin array baru, kalau cukup dia replace
	sliceAppend[3] = "bukan"
	fmt.Println(sliceAppend)
	fmt.Println(arr)
	
	newSlice[0] = "1";
	newSlice[1] = "2";
	fmt.Println(newSlice)
	//copy old slice to new slice
	copySlice := make([]string, len(newSlice), cap(newSlice))
	copy(copySlice, newSlice)
	fmt.Println(copySlice)
}