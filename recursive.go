package main

import "fmt"

func squaredRecursive(value int, factorial int)int{
	if(factorial ==0){
		return 1
	} else {
		return value * squaredRecursive(value, factorial-1)
	} 
}
func logRecursive(value int, result int, i int) int {
	if(result == 1 && i == 0){
		return 0
	} else if result ==1{
		return i
	} else {
		result:= result/value
		return  logRecursive(value, result, i+1)
	}
}
func factorialRecursive(value int) int {
    if value == 1 {
        return 1
    } else {
        return value * factorialRecursive(value-1)
    }
}
func main() {
	fmt.Println(squaredRecursive(3, 4))
	fmt.Println(factorialRecursive(3))
	fmt.Println(logRecursive(7, 16807, 0))
}
