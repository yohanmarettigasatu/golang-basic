package main

import "fmt"

type Address struct {
	city, province, country string
}

func main() {
	var address1 Address = Address{"cianjur", "jawa barat", "indonesia"}
	var address2 *Address = &address1
	var address3 *Address = &address1
	// when change field in pointer all data who reference will change to
	address1.city = "bandung"
	address2.city = "subang"
	// when we change variable in pointer just data in pointer has change
	address2 = &Address{"tangerang", "banten", "indonesa"}
	fmt.Println(address1)
	fmt.Println(address2)
	fmt.Println("-----------")
	*address3 = Address{"serang", "banten", "indonesa"}
	fmt.Println(address1)
	fmt.Println(address3)
	//notice !!! if you first change pointer without * your pointer will go out 
	//and then when you change pointer again with * its not change another var because the pointer has out reference
	var address4 *Address = new(Address);
	address4.city = "jakarta"
	fmt.Println(address4)
	fmt.Println("============")

	//passing data normal to func
	changeCountryTo(address1);
	fmt.Println(address1)
	fmt.Println("============")

	//passing data pointer to func
	pointerChangeCountryTo(&address1);
	pointerChangeCountryTo(address2);

	fmt.Println(address1)
	fmt.Println(address2)
	//whyy????
	//because if you using normal func in func who manipulated huge data your func will duplicate the data make your memory huge to
	//method pointer
	address1.changecity()
	fmt.Println(address1)
	//note if you using struct is better using pointer too
}

func changeCountryTo(address Address){
	address.country ="singapore"
	fmt.Println(address)
}
func pointerChangeCountryTo(address *Address){
	address.country ="singapore"
	fmt.Println(address)
}

func (address *Address)changecity(){
	address.city = "uknown"
}