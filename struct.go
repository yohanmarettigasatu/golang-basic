package main

import "fmt"

// struct in golang like object in another language but you must define
type User struct{
	name, address string
	age int
	married bool
}
func (user User) sayHello()string{
	return "hello my name is " + user.name
}
func profile(age int, status bool,data ...string)(User, User, User){
	var admin User
	fmt.Println(admin)

	admin.age = age
	admin.name = data[0]
	admin.address = data[1]
	admin.married = status
	yohan := User{
		name: "yohan",
		address: "tangerang",
		age: 20,
		married: false,
	}
	budi:= User{"budi","tangerang",20,true}
	
	return admin, yohan , budi
}
func main() {
	admin, yohan, budi := profile(20, false, "admin","indo")
	fmt.Println(admin)
	fmt.Println(yohan)
	fmt.Println(budi)
	fmt.Println(admin.sayHello())
	fmt.Println(yohan.sayHello())
	fmt.Println(budi.sayHello())
}
