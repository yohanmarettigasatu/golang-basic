package main

import (
	"fmt"
	"os"
	"packageimportlib/database"
	"sort"
	"strings"

	// use blank identifier if you not use func in package its use if you just want running init func in some package
	_ "packageimportlib/help"
)
type User struct{
	Name string
	Age int
}
type UserSlice []User
func main(){
	// data := help.SayGoodbye()
	// fmt.Println("haii")
	// fmt.Println("data", data)
	// fmt.Println("bye", help.App)
	database := database.GetDatabase()
	fmt.Println(strings.ToUpper(database))
	argumens := os.Args
	hostname, error := os.Hostname()
	fmt.Println(argumens[0])
	fmt.Println(hostname)
	fmt.Println(error)	
	users := []User {
		{"yohan", 20},
		{"budi", 33},
		{"rud", 40},
		{"lez", 24},
	}
	sort.Sort(UserSlice(users))
	fmt.Println(users)
}

func (value UserSlice) Len() int {
	return len(value)
}

func (value UserSlice) Less(i, j int) bool {
	return value[i].Age < value[j].Age
}

func (value UserSlice) Swap(i, j int) {
	value[i], value[j] = value[j], value[i]
}