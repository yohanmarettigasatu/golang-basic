package main

import "fmt"

func main(){
	//make map before initiated
	book := make(map[string]string)
	//make map before initiated
	obj := map[string]string{
		"name": "yohan",
		"age": "4",
	}
	fmt.Println(obj["name"])
	fmt.Println(obj["age"])
	obj["title"] = "dev"
	fmt.Println(obj["title"])
	
	book["title"] = "booke1"
	book["author"] = "yohan"
	book["publish"] = "indo"
	fmt.Println(book["publish"])
	delete(book, "publish")
	fmt.Println(book["publish"])
}