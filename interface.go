package main

import (
	"errors"
	"fmt"
)
type HasName interface{
	getName() string
}
func sayHello(hasName HasName){
	fmt.Println("hello, ", hasName.getName());
}

type Human struct{
	name string
};

func (human Human) getName() string {
	return human.name
}

func main() {
	var yohan Human;
	yohan.name ="yohan martisa"	
	fmt.Println(yohan)
	sayHello(yohan)
	var in interface{} = interNull(1)
	data := interNull(10)
	fmt.Println(data)
	fmt.Println(in)
	var contohError error = errors.New("interface error")
	fmt.Println(contohError.Error());
	nilai, err := pembagian(2,0)
	if(err == nil){
		fmt.Println("hasil", nilai)
	}else{
		fmt.Println("error", err.Error())
	}
	//assertion
	var asser interface{} = random()
	var result1 = asser.(string)
	fmt.Println(result1)
	// tidak sesuai output: panic
	// var result2 = asser.(int)
	// fmt.Println(result2)
	switch switchAssertion := asser.(type) {
	case string:
		fmt.Println("string", switchAssertion)
	case int:
		fmt.Println("integer", switchAssertion)
	default:
		fmt.Println("uknown")
	}
	

}

func interNull(i int)interface{}{
	if(i == 0){
		return nil
	} else if(i==1){
		return i
	} else {
		return "all numbers positive"
	}
}

func pembagian(i int, pembagi int)(int, error){
	if(pembagi ==0){
		return 0, errors.New("pembagi tidak boleh 0")
	}else{
		return i/pembagi, nil
	}
}

func random () interface{}{
	return "assertion"
}